package com.devsuperior.hrpayroll.resouces;

import com.devsuperior.hrpayroll.entities.Payment;
import com.devsuperior.hrpayroll.services.PaymentService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/payments")
public class PaymentResouce {

    @Autowired
    private PaymentService paymentService;

    @HystrixCommand(fallbackMethod = "getPaymentAlternative")
    @GetMapping("/{id}/days/{days}")
    public ResponseEntity<Payment> getPaymentId(@PathVariable Long id,
                                                @PathVariable int days){
        return ResponseEntity.ok(paymentService.getPayment(id,days));
     }

    public ResponseEntity<Payment> getPaymentAlternative(Long id, int days){
        Payment payment = new Payment("Mario", 500.0 ,days);
        return ResponseEntity.ok(payment);

    }
}

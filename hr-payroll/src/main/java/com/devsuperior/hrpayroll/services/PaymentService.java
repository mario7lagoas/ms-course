package com.devsuperior.hrpayroll.services;

import com.devsuperior.hrpayroll.entities.Payment;
import com.devsuperior.hrpayroll.entities.Worker;
import com.devsuperior.hrpayroll.feignclients.WorkerFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {
 /*
    @Value("${hr-worker.host}")
    private String workHost;

    @Autowired
    private RestTemplate restTemplate;

 */

    @Autowired
    private WorkerFeignClient workerFeignClient;

    public Payment getPayment(Long workerId, int days) {
      /* isso com RestTemplate

        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("id", workerId.toString());

        Worker worker = restTemplate.getForObject(workHost + "/workers/{id}", Worker.class, uriVariables);

       */


        Worker worker = workerFeignClient.findId(workerId).getBody();

        return new Payment(worker.getName(), worker.getDailyIncome(), days);
    }

}

package com.devsuperior.hruser.services;

import com.devsuperior.hruser.entities.User;
import com.devsuperior.hruser.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User findById(Long id) {
        User user = userRepository.findById(id).get();
        if (user != null){
            return user;
        }
        return null;
    }

    public User findByEmail(String email) {
        User user = userRepository.findByEmail(email);
        if (user != null){
            return user;
        }
        return null;
    }
}

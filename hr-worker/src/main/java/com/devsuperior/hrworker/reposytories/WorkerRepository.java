package com.devsuperior.hrworker.reposytories;

import com.devsuperior.hrworker.entities.Worker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface WorkerRepository extends JpaRepository<Worker, Long> {
}
